import axios from 'axios';

const instance = () => {
    const instance = axios.create({
        baseURL: process.env.REACT_APP_BASE_API
    });

    if(localStorage.getItem('token-netgrid'))
        instance.defaults.headers.common['Authorization'] = `Bearer ${localStorage.getItem('token-netgrid')}`;

    instance.interceptors.request.use(function (config) {
        let token = localStorage.getItem('token-netgrid');
    
        config.headers['Authorization'] = token ? `Bearer ${token}` : null;
        return config;
    }, function (error) {
        return Promise.reject(error);
    });
    return instance
}

export default instance();