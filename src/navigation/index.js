import React, { useContext } from 'react';
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import { AuthContext } from '../provider';
import Login from '../pages/Login';
import Register from '../pages/Register';
import Home from '../pages/Home';
import Pokemon from '../pages/Pokemon';
import Profile from '../pages/Profile';

const RequiredAuth = ({ children, user }) => {
    if(!user) return <Navigate to="/auth/login" replace />;
    return children;
}

const Navigation = () => {
    const auth = useContext(AuthContext);
	const user = auth.user;

    return(
        <BrowserRouter>
            <Routes>
                <Route index path="/auth/login" element={<Login />} />
                <Route path="/auth/register" element={<Register />} />
                <Route 
                    path="/user/home" 
                    element={
                        <RequiredAuth user={user}>
                            <Home />
                        </RequiredAuth>
                    } 
                />
                <Route 
                    path="/user/pokemon/:id" 
                    element={
                        <RequiredAuth user={user}>
                            <Pokemon />
                        </RequiredAuth>
                    } 
                />
                <Route 
                    path="/user/profile" 
                    element={
                        <RequiredAuth user={user}>
                            <Profile />
                        </RequiredAuth>
                    } 
                />
                <Route
                    path="*"
                    element={
                    !user ?
                        <Navigate to="/auth/login" replace /> :
                        <Navigate to="/user/home" replace />
                    }
                />
            </Routes>
        </BrowserRouter>
    )
}

export default Navigation;