import React, { useState } from "react";
import { Link, useNavigate } from 'react-router-dom';
import ContainerAuth from "../components/ContainerAuth";
import Label from "../components/Label";
import Input from '../components/Input';
import Button from '../components/Button';
import axios from '../axios.config';
import { connect } from 'react-redux';
import * as actions from '../redux/actions/auth-actions';

const Register = (props) => {
    const navigate = useNavigate();
    const [defaultValues, setDefaultValues] = useState({
        name: '',
        email: '',
        password: '',
    })

    const onSubmit = async (e) => {
        e.preventDefault();

        const { data, status } = await axios.post('/register', { ...defaultValues });
        if(status === 200 && data){
            props.loginSuccess(data.data.user, data.data.access_token.token)
            navigate('/user/home');
        }
    }

    const onChange = ({ target: { name, value } }) => {
        setDefaultValues({
            ...defaultValues,
            [name]: value
        })
    }

    return (
        <ContainerAuth>
            <form className="form-container" onSubmit={onSubmit}>
                <h4 className="text-center font-weight-bold">Registrarme</h4>

                <div className="form-group">
                    <Label forhtml="InputName">Nombre</Label>
                    <Input type="text" className="form-control" id="InputName" name="name" onChange={onChange} value={defaultValues.name} placeholder="Ingresa tu nombre" required />
                </div>
                <div className="form-group">
                    <Label forhtml="InputEmail1">Correo electronico</Label>
                    <Input type="email" className="form-control" id="InputEmail1" name="email" onChange={onChange} value={defaultValues.email} placeholder="Ingresa tu correo" required />
                </div>
                <div className="form-group">
                    <Label forhtml="InputPassword1">Contraseña</Label>
                    <Input type="password" className="form-control" id="InputPassword1" name="password" onChange={onChange} value={defaultValues.password} placeholder="Ingresa tu contraseña" min="2" required />
                </div>

                <div className="d-grid gap-2 mt-2">
                    <Button type="submit" className="btn btn-primary">Registrarme</Button>
                </div>
                <div className="form-footer text-center">
                    <p> Ya tienes una cuenta? <Link to={'/auth/login'}>Inicia sesión</Link></p>
                </div>
            </form>
        </ContainerAuth>
    )
}

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({
	loginSuccess: (data, token) => dispatch(actions.loginSuccess(data, token)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);