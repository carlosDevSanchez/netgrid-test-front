import React, { useEffect, useState } from "react";
import Navbar from "../components/Navigation/Navbar";
import Spinner from '../components/Spinner';
import axios from 'axios';
import CardPokemon from "../components/CardPokemon";

const Home = () => {
    const [pokemones, setPokemones] = useState([]);
    const [loading, setLoading] = useState(false);
    const [urls, setUrls] = useState({
        init: process.env.REACT_APP_POKE_API,
        next: null,
        prev: null
    });

    const onInit = async (url) => {
        setLoading(true);
        const { data, status } = await axios.get(url);
        if(status === 200 && data){
            setPokemones(data.results)
            setUrls({
                ...urls,
                next: data.next,
                prev: data.previous
            })
            setLoading(false);
        }
    }

    useEffect(() => {
        const onInit = async (url) => {
            setLoading(true);
            const { data, status } = await axios.get(url);
            if(status === 200 && data){
                setPokemones(data.results)
                setUrls({
                    init: process.env.REACT_APP_POKE_API,
                    next: data.next,
                    prev: data.previous
                })
                setLoading(false);
            }
        }
        onInit(process.env.REACT_APP_POKE_API);
    },[])

    return(
        <section>
            <Navbar />
            <div className="container">
                {loading && <Spinner />}
                <div className="row">
                    {(!loading && pokemones.length > 0) &&
                        pokemones.map((pk, k) => (
                            <CardPokemon
                                key={k}
                                url={pk.url}
                            />
                        ))
                    }
                </div>
                <div className="row justify-content-center my-4">
                    <div className="col-md-12 text-center">
                        <button type="button" disabled={!urls.prev} onClick={() => onInit(urls.prev)} className="btn btn-danger mx-2">Anterior</button>
                        <button type="button" disabled={!urls.next} onClick={() => onInit(urls.next)} className="btn btn-success mx-2">Siguiente</button>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Home;