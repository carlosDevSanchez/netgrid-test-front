import React, { useState } from "react";
import { Link, useNavigate } from 'react-router-dom';
import ContainerAuth from "../components/ContainerAuth";
import Label from "../components/Label";
import Input from '../components/Input';
import Button from '../components/Button';
import axios from "../axios.config";
import { connect } from 'react-redux';
import * as actions from '../redux/actions/auth-actions';

const Login = (props) => {
    const navigate = useNavigate();
    const [defaultValues, setDefaultValues] = useState({
        email: '',
        password: ''
    })

    const onSubmit = async (e) => {
        e.preventDefault();

        const { data, status } = await axios.post('/login', { ...defaultValues });
        if(status === 200 && data){
            props.loginSuccess(data.data.user, data.data.access_token.token);
            navigate('/user/home');
        }
    }

    const onChange = ({ target: { name, value } }) => {
        setDefaultValues({
            ...defaultValues,
            [name]: value
        })
    }

    return (
        <ContainerAuth>
            <form className="form-container" onSubmit={onSubmit}>
                <h4 className="text-center font-weight-bold">Iniciar sesión</h4>
                <div className="form-group">
                    <Label forhtml="InputEmail1">Correo electronico</Label>
                    <Input type="email" className="form-control" id="InputEmail1" name="email" value={defaultValues.email} onChange={onChange} placeholder="Ingresa tu correo" />
                </div>
                <div className="form-group">
                    <Label forhtml="InputPassword1">Contraseña</Label>
                    <Input type="password" className="form-control" id="InputPassword1" name="password" value={defaultValues.password} onChange={onChange} placeholder="Ingresa tu contraseña" />
                </div>
                <div className="d-grid gap-2 mt-2">
                    <Button type="submit" className="btn btn-primary">Iniciar</Button>
                </div>
                <div className="form-footer text-center">
                    <p> No tienes una cuenta? <Link to={'/auth/register'}>Registrate</Link></p>
                </div>
            </form>
        </ContainerAuth>
    )
}

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({
	loginSuccess: (data, token) => dispatch(actions.loginSuccess(data, token)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);