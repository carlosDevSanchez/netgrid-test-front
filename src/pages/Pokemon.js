import React from "react";
import Navbar from '../components/Navigation/Navbar';
import { useParams } from 'react-router-dom';
import CardPokemon from "../components/CardPokemon";

const Pokemon = () => {
    const { id } = useParams();
    return(
        <section>
            <Navbar />
            <div className="container">
                <div className="row justify-content-center">
                    <CardPokemon
                        url={`https://pokeapi.co/api/v2/pokemon/${id}`}
                        details={true}
                    />
                </div>
            </div>
        </section>
    )
}

export default Pokemon;