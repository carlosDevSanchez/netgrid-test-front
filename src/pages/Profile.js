import React, { useEffect, useState } from "react";
import Label from "../components/Label";
import Input from '../components/Input';
import Button from '../components/Button';
import Navbar from "../components/Navigation/Navbar";
import axiosConfig from "../axios.config";

const Profile = (props) => {
    const [defaultValues, setDefaultValues] = useState({
        name: '',
        birthdate: '',
        email: '',
        address: '',
        city: ''
    })

    const onInit = async () => {
        const { data, status } = await axiosConfig.get('/user');
        if(status === 200 && data){
            setDefaultValues({
                name: data.data.name,
                birthdate: data.data.birthdate,
                email: data.data.email,
                address: data.data.address,
                city: data.data.city
            })
        }
    }

    const onSubmit = async (e) => {
        e.preventDefault();

        const { data, status } = await axiosConfig.put('/update-user', { ...defaultValues });
        if(status === 200 && data){
            alert('Actualizado correctamente!');
        }
    }

    const onChange = ({ target: { name, value } }) => {
        setDefaultValues({
            ...defaultValues,
            [name]: value
        })
    }

    useEffect(() => {
        onInit();
    },[])

    return (
        <section>
            <Navbar />
            <div className="container">
                <div className="row ">
                    <div className="col-md-6 mt-2">
                        <form className="form-container" onSubmit={onSubmit}>
                            <div className="form-group">
                                <Label forhtml="InputName">Nombre</Label>
                                <Input type="text" className="form-control" id="InputName" name="name" onChange={onChange} value={defaultValues.name} placeholder="Ingresa tu nombre" required />
                            </div>
                            <div className="form-group">
                                <Label forhtml="Inputdate">Fecha de nacimiento</Label>
                                <Input type="date" className="form-control" id="Inputdate" name="birthdate" onChange={onChange} value={defaultValues.birthdate} placeholder="Ingresa tu fecha de nacimiento" required />
                            </div>

                            <div className="form-group">
                                <Label forhtml="InputEmail1">Correo electronico</Label>
                                <Input type="email" className="form-control" id="InputEmail1" name="email" onChange={onChange} value={defaultValues.email} placeholder="Ingresa tu correo" required />
                            </div>
                            <div className="form-group">
                                <Label forhtml="Inputaddress">Dirección</Label>
                                <Input type="text" className="form-control" id="Inputaddress" name="address" onChange={onChange} value={defaultValues.address} placeholder="Ingresa tu dirección" required />
                            </div>
                            <div className="form-group">
                                <Label forhtml="InputCity">Ciudad</Label>
                                <Input type="text" className="form-control" id="InputCity" name="city" onChange={onChange} value={defaultValues.city} placeholder="Ingresa tu ciudad" required />
                            </div>

                            <div className="d-grid gap-2 mt-2">
                                <Button type="submit" className="btn btn-primary">Actualizar</Button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Profile;