export const loginSuccess = (userData, token) => {
    return{
        type:'LOGIN_SUCCESS',
        userData,
        token
    }
}

export const updateData = (data) => {
  return {
    type: 'UPDATE_DATA',
    userData: data
  }
}

export const logout = ( ) => {
  return {
    type: 'LOGOUT',
  }
}