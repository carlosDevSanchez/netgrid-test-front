const initState = {
    isLoggedIn: localStorage.getItem('token-netgrid') ? true : false,
    token: localStorage.getItem('token-netgrid') ? localStorage.getItem('token-netgrid') : null,
    userData: localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : {},
}

const auth = (state = initState, action) => {
    switch (action.type) {
        case 'LOGIN_SUCCESS':
            localStorage.setItem('token-netgrid', action.token)
            localStorage.setItem('user', JSON.stringify(action.userData));
            return{
                ...state,
                isLoggedIn: true,
                userData: action.userData,
                token: action.token,
            }
        case 'UPDATE_DATA':
            localStorage.setItem('user', JSON.stringify(action.userData));
            return {
                ...state,
                userData: action.userData,
            }
        case 'LOGOUT':
            localStorage.clear();
            return{
                ...state,
                isLoggedIn:false,
                token: null
            }
        default:
            return state
    }
}

export default auth;