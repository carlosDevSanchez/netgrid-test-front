import { configureStore } from '@reduxjs/toolkit';
import Reducers from './reducers';

const configure = () => {
    let store = configureStore({ reducer: Reducers });
    return store;
};

export const store = configure();