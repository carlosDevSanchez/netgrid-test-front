import React from "react";

const ContainerAuth = ({ children }) => {
    return (
        <section className="container-fluid">
            <section className="row align-items-center justify-content-center" style={{minHeight: '100vh'}}>
                <section className="col-xl-4 col-lg-6 col-md-6 col-sm-8">
                    {children}
                </section>
            </section>
        </section>
    )
}
export default ContainerAuth;