import React, { useState, useEffect } from "react";
import { Link } from 'react-router-dom';
import Spinner from "./Spinner";
import axios from 'axios';
import axiosConfig from "../axios.config";

const CardPokemon = ({ url, details }) => {
    const [loading, setLoading] = useState(true);
    const [pokemon, setPokemon] = useState({});
    const [favorite, setFavorite] = useState(false);

    const getFavorite = async () => {
        const { data, status } = await axiosConfig.get(`/favorite/${pokemon.id}`);
        if(status === 200 && data?.data?.favorite){
            setFavorite(true);
        }else{
            setFavorite(false);
        }
    }

    const addFavoritePokemon = async () => {
        const { data, status } = await axiosConfig.post('/add-favorite', { id: pokemon.id });
        if(status === 200 && data){
            getFavorite();
            alert('Agregado correctamente!');
        }
    }

    const removeFavoritePokemon = async () => {
        const { data, status } = await axiosConfig.delete(`/remove-favorite/${pokemon.id}`);
        if(status === 200 && data){
            getFavorite();
            alert('Eliminado correctamente!');
        }
    }

    useEffect(() => {
        const getPokemonData = async () => {
            const { status, data } = await axios.get(url);
            if(status === 200 && data){
                setPokemon(data);
                setLoading(false);
            }
        }
        getPokemonData();
    },[url])

    useEffect(() => {
        if(pokemon.id && details){
            const getFavorite = async () => {
                const { data, status } = await axiosConfig.get(`/favorite/${pokemon.id}`);
                if(status === 200 && data?.data?.favorite){
                    setFavorite(true);
                }else{
                    setFavorite(false);
                }
            }
            getFavorite();
        }
    },[pokemon, details])
    
    if(loading)
        return <Spinner />

    return(
        <div className={details ? 'col-lg-6 col-sm-12 my-2' : "col-lg-3 col-md-6 col-sm-12 my-2"}>
            <div className="card">
                <img src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/${pokemon.id}.svg`} style={{maxHeight: 100}} className="card-img-top" alt={pokemon.name} />
                <div className="card-body">
                    <h5 className="card-title">{pokemon.name}</h5>
                    {details ?
                        <div>
                            <ul>
                                <li>Experiencia: {pokemon.base_experience}</li>
                                <li>Altura: {pokemon.height}</li>
                                <li>Peso: {pokemon.weight}</li>
                                <li>Habilidades: {pokemon.abilities.map(hb => hb.ability.name + ', ')}</li>
                            </ul>
                            <Link to={`/user/home`} className="btn btn-danger">Volver</Link>

                            {!favorite ?
                                <div onClick={addFavoritePokemon} className="btn btn-primary mx-2">Añadir favoritos</div>
                            :
                                <div onClick={removeFavoritePokemon} className="btn btn-secondary mx-2">Remover favoritos</div>
                            }
                        </div>
                    :
                        <Link to={`/user/pokemon/${pokemon.id}`} className="btn btn-primary">Detalles</Link>
                    }
                </div>
            </div>
        </div>
    )
}

export default CardPokemon;