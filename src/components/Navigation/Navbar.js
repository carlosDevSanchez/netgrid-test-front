import React from 'react';
import { Link } from 'react-router-dom';
import NavItem from './NavItem';

const items = [
    {
        name: 'Pokemones',
        link: '/user/home'
    },
    {
        name: 'Perfil',
        link: '/user/profile'
    }
]

const Navbar = () => {
    return(
        <nav className="navbar navbar-expand-lg bg-primary" data-bs-theme="dark">
            <div className="container">
                <Link className="navbar-brand text-white" to="">PokeApi</Link>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
                        {items.map((row, k) => (
                            <NavItem key={k} link={row.link}>{row.name}</NavItem>
                        ))}
                    </ul>
                </div>
            </div>
        </nav>
    )
}

export default Navbar;