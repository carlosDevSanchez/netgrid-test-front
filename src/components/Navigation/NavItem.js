import React from "react";
import { Link } from 'react-router-dom';

const NavItem = (props) => {
    return (
        <li className="nav-item">
            <Link className="nav-link text-white" aria-current="page" to={props.link}>{props.children}</Link>
        </li>
    )
}

export default NavItem;