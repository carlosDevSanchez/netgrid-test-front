import React from "react";
const Label = (props) => (<label {...props}>{props.children}</label>)
export default Label;